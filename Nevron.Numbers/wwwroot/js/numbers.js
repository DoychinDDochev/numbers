﻿const notSummed = 'Not Summed';
const getUrl = 'https://localhost:44305/home/get';
const addUrl = 'https://localhost:44305/home/add';
const sumUrl = 'https://localhost:44305/home/sum';
const clearUrl = 'https://localhost:44305/home/clear';

//list
$(document).ready(function () {
    var flickerAPI = getUrl;
    $("#numbers").empty(); //clear

    $.getJSON(flickerAPI, {
        format: "json"
    })
        .done(function (data) {
            $("#count").text(data.length);

            $.each(data, function (i, item) {
                display(item);
            });
        });   

    $("#sum").text(notSummed);
});

//add
$("#btnAdd").click(function () {
    var flickerAPI = addUrl;
    $("#numbers").empty(); //clear

    $.getJSON(flickerAPI, {
        format: "json"
    })
        .done(function (data) {
            $("#count").text(data.length);

            $.each(data, function (i, item) {
                display(item);
            });
        });

    $("#sum").text(notSummed);
});

//clear
$("#btnClear").click(function () {
    var flickerAPI = clearUrl;
    $("#numbers").empty();
    $("#count").text(0);

    $.getJSON(flickerAPI, {
        format: "json"
    });

    $("#sum").text(notSummed);
});

//sum
$("#btnSum").click(function () {
    var flickerAPI = sumUrl;
    
    $.getJSON(flickerAPI, {
        format: "json"
    })
        .done(function (data) {
            $("#sum").text(data);
        });
});

function display(item) {
    $("<div>").addClass("numItem").text(item).appendTo("#numbers"); 
}