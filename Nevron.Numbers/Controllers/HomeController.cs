﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Nevron.Numbers.Extensions;
using Nevron.Numbers.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Nevron.Numbers.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private string sessionKeyNumbers = "nums";

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }



        public IActionResult Get()
        {
            CheckState();

            var list = HttpContext.Session.Get<List<int>>(sessionKeyNumbers);

            return Json(list);
        }

        public IActionResult Add()
        {
            CheckState();

            var list = HttpContext.Session.Get<List<int>>(sessionKeyNumbers);

            Random random = new Random();
            int value = random.Next(1, 1000);
            list.Add(value);
            HttpContext.Session.Set<List<int>>(sessionKeyNumbers, list);

            return Json(list);
        }

        public IActionResult Clear()
        {
            HttpContext.Session.Clear();

            CheckState();

            var list = HttpContext.Session.Get<List<int>>(sessionKeyNumbers);

            return Json(list);
        }

        public IActionResult Sum()
        {
            CheckState();

            var list = HttpContext.Session.Get<List<int>>(sessionKeyNumbers);
            int sum = list.Sum();

            return Json(sum);
        }

        private void CheckState()
        {
            List<int> numArray = new List<int>();
            if (HttpContext.Session.Get<List<int>>(sessionKeyNumbers) == default(List<int>))
            {
                HttpContext.Session.Set<List<int>>(sessionKeyNumbers, numArray);
            }
        }

    }

   
}
